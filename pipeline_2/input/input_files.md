Pipeline input files
====================

The following tree diagram lists input files for the pipeline.

1. Files followed by the arrow (`->`) are symbolic links to other
   files.

2. Directories *circos* and *genes* and the FASTA file of the
   previously studied fragments of the jaguarundi mitochondrial genome
   (*jaguarundi\_mt\_seqs.fa*) are given in this repository.

3. Files in directories *bams*, *variants* and *windowmasker* are
   obtained using *pipeline_1* from this repository.

4. Files which names start with *GCF* or *GCA* can be downloaded from
   NCBI.

5. Other files are available from the Dryad repository of the paper.

```
.
├── bams
│   ├── ref.filtered.bam
│   ├── ref.filtered.bam.bai
│   ├── reseq.SRR6071635.filtered.bam
│   └── reseq.SRR6071635.filtered.bam.bai
├── cat
│   └── GCF_000181335.3_Felis_catus_9.0_genomic.fna
├── cheetah
│   ├── assembly.fna -> GCF_003709585.1_Aci_jub_2_genomic.fna
│   ├── assembly.fna.fai
│   ├── full_table.tsv -> Rico_full_table.tsv
│   ├── GCF_003709585.1_Aci_jub_2_genomic.fna
│   ├── GCF_003709585.1_Aci_jub_2_rm.out.gz
│   ├── repeatmasker.out.gz -> GCF_003709585.1_Aci_jub_2_rm.out.gz
│   └── Rico_full_table.tsv
├── circos
│   ├── ideogram.conf
│   ├── karyotype.txt
│   ├── mt_features.txt
│   ├── mt_genome.conf
│   └── ticks.conf
├── genes
│   ├── cds.fa
│   ├── genes.gtf
│   └── protein.fa
├── jaguarundi
│   ├── assembly.fna -> GCA_014898765.1_PumYag_genomic.fna
│   ├── assembly.fna.fai -> GCA_014898765.1_PumYag_genomic.fna.fai
│   ├── Contamination.txt
│   ├── full_table.tsv -> PumYag_full_table.tsv
│   ├── GCA_014898765.1_PumYag_genomic.fna
│   ├── GCA_014898765.1_PumYag_rm.out.gz
│   ├── PumYag_full_table.tsv
│   ├── repeatmasker.out.gz -> GCA_014898765.1_PumYag_rm.out.gz
│   ├── supernova_pseudohap2.1.fasta
│   └── supernova_pseudohap2.2.fasta
├── jaguarundi_mt_seqs.fa
├── NC_028311.1.fa
├── puma
│   ├── assembly.fna -> GCF_003327715.1_PumCon1.0_genomic.fna
│   ├── assembly.fna.fai
│   ├── full_table.tsv -> PumCon_full_table.tsv
│   ├── GCF_003327715.1_PumCon1.0_genomic.fna
│   ├── GCF_003327715.1_PumCon1.0_rm.out.gz
│   ├── PumCon_full_table.tsv
│   └── repeatmasker.out.gz -> GCF_003327715.1_PumCon1.0_rm.out.gz
├── variants
│   ├── cheetah.ref.vcf
│   ├── cheetah.reseq.vcf
│   ├── jaguarundi.ref.vcf
│   ├── jaguarundi.reseq.vcf
│   ├── puma.ref.vcf
│   └── puma.reseq.vcf
└── windowmasker
	└── GCA_014898765.1_PumYag_genomic.mt.wm
```
