import jdga.fasta
import jdga.sequence


def write_gaps_bed(fasta_path, bed_path):
    """Write the BED file of gap regions for sequences in a FASTA file."""
    records = [
        jdga.fasta.remove_comment(jdga.fasta.remove_soft_mask(k))
        for k in jdga.fasta.read(fasta_path)
    ]
    with open(bed_path, 'wt') as bed_file:
        for name, seq in records:
            for start, end in jdga.sequence.gap_regions(seq):
                print(str.format('{:s}\t{:d}\t{:d}', bytes.decode(name), start,
                                 end),
                      file=bed_file)


if __name__ == '__main__':
    write_gaps_bed(snakemake.input[0], snakemake.output[0])
