import jdga.windowmasker


def convert_wm_to_bed(wm_path, bed_path):
    """Convert a WindowMasker interval file to the BED format."""
    jdga.windowmasker.write_bed(jdga.windowmasker.read_intervals(wm_path),
                                bed_path)


if __name__ == '__main__':
    convert_wm_to_bed(snakemake.input[0], snakemake.output[0])
