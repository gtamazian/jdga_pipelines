library(tidyverse)

read_variants <- function(path) {
    read_tsv(path,
        comment = "#",
        col_names = c("scaffold", "ref", "alt", "info", "sample"),
        col_types = c("c--cc--c-c")
    ) %>%
        mutate(
            num_alleles = str_count(alt, fixed(",")) + 1,
            var_type = str_extract(info, "TYPE=.*;") %>%
                str_remove(fixed("TYPE=")) %>%
                str_remove(fixed(";")),
            genotype = str_extract(sample, "[^:]*")
        ) %>%
        select(-c("info", "sample"))
}

read_biallelic_variants <- function(path) {
    read_variants(path) %>%
        filter(num_alleles == 1 & var_type == "snp" & genotype == "0/1")
}

read_scaffold_list <- function(path) {
    read_tsv(path, col_names = c("scaffold"), col_types = c("c")) %>%
        .$scaffold
}

get_excluded_scaffolds <- function(x_scaffolds_path, mt_scaffold) {
  tibble(
    scaffold = c(read_scaffold_list(x_scaffolds_path), mt_scaffold),
    label = "X"
  )
}

filter_by_scaffolds <- function(variants, excluded_scaffolds) {
    variants %>%
        left_join(excluded_scaffolds) %>%
        filter(is.na(label)) %>%
        select(-label)
}

load_variants <- function(vcf_path, x_scaf_path, mt_seqid) {
    excluded <- get_excluded_scaffolds(x_scaf_path, mt_seqid)

    filter_by_scaffolds(
        read_biallelic_variants(vcf_path),
        excluded
    )
}

count_tstv <- function(variants, out_path) {
    transitions <- tibble(
        ref = c("A", "G", "C", "T"),
        alt = c("G", "A", "T", "C"),
        type = "transition"
    )

    variants %>%
        left_join(transitions) %>%
        mutate(type = replace_na(type, "transversion")) %>%
        count(type) %>%
        write_tsv(file = out_path)
}

count_tstv(
  load_variants(
    snakemake@input[["vcf"]],
    snakemake@input[["x_scaffolds"]],
    snakemake@params[["mt"]]
  ), snakemake@output[[1]]
)
