#!/usr/bin/env python3

import jdga.fasta
import jdga.sequence


def process_raw_assembly(path, out_gaps, out_dups):
    sequences = [jdga.fasta.remove_comment(k) for k in jdga.fasta.read(path)]
    gaps = [k.name for k in sequences if jdga.sequence.is_gap_sequence(k.seq)]
    duplicates = jdga.fasta.duplicates(
        [k for k in sequences if not jdga.sequence.is_gap_sequence(k.seq)])
    seq_lengths = {k.name: len(k.seq) for k in sequences}

    with open(out_gaps, 'wt') as gaps_file:
        for k in gaps:
            gaps_file.write(
                str.format('{:s}\t{:d}\n', bytes.decode(k), seq_lengths[k]))

    with open(out_dups, 'wt') as dups_file:
        for j in duplicates:
            for k in j[1:]:
                dups_file.write(
                    str.format('{:s}\t{:d}\n', bytes.decode(k),
                               seq_lengths[k]))


if __name__ == '__main__':
    process_raw_assembly(snakemake.input[0], snakemake.output['gaps'],
                         snakemake.output['dups'])
