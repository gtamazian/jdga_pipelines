#!/usr/bin/env python3

import jdga.conrep

COLUMNS = ['SEQUENCE', 'START', 'END', 'SEQ_LEN', 'SOURCE']


def print_trimmed_regions(in_path, out_path):
    """Print trimmed regions of sequences."""
    report = jdga.conrep.read(in_path)

    with open(out_path, 'wt') as out_file:
        print(str.join('\t', COLUMNS), file=out_file)
        for seq in report.trimmed:
            for region in seq.span:
                print(str.format('{:s}\t{:d}\t{:d}\t{:d}\t{:s}', seq.name,
                                 region[0], region[1], seq.length, seq.source),
                      file=out_file)


if __name__ == '__main__':
    print_trimmed_regions(snakemake.input[0], snakemake.output[0])
