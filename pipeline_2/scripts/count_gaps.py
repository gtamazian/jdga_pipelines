#!/usr/bin/env python3

import jdga.conrep
import jdga.fasta
import jdga.sequence


def gaps_counts(records, excluded):
    """Count the total number of gaps in sequence records."""
    return [
        sum(map(jdga.sequence.is_gap, k.seq)) for k in records
        if k.name not in excluded
    ]


def read_excluded(report_path):
    """Get excluded sequences from the NCBI Contamination report."""
    report = jdga.conrep.read(report_path)
    return [str.encode(k.name) for k in report.excluded]


def read_names(path):
    """Read sequence names from a file."""
    with open(path) as name_file:
        return [str.encode(str.split(str.rstrip(k))[0]) for k in name_file]


def process_assembly(fasta_path, report_path, dups_path, gaps_path, out_path):
    """Print the number of sequences and the total number of gaps."""
    sequences = [
        jdga.fasta.remove_comment(jdga.fasta.remove_soft_mask(k))
        for k in jdga.fasta.read(fasta_path)
    ]
    excluded = set.union(set(read_excluded(report_path)),
                         set(read_names(dups_path)),
                         set(read_names(gaps_path)))

    gaps = gaps_counts(sequences, excluded)

    with open(out_path, 'wt') as out_file:
        print(str.format('{:d} sequences, {:d} bp of gaps', len(gaps),
                         sum(gaps)),
              file=out_file)


if __name__ == '__main__':
    process_assembly(snakemake.input['fasta'], snakemake.input['cont_rep'],
                     snakemake.input['dups'], snakemake.input['gaps'],
                     snakemake.output[0])
