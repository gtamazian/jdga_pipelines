#!/usr/bin/env python3

from typing import List, Tuple

import jdga.assembly
import jdga.fasta
import jdga.sequence

SeqRecord = List[Tuple[str, int, float, int]]

_COLUMNS = ['label', 'sequence', 'length', 'gc_content', 'gaps']


def seq_stats(path: str) -> List[SeqRecord]:
    """Get statistics for sequences from a FASTA file."""
    sequences = [
        jdga.fasta.remove_comment(jdga.fasta.remove_soft_mask(k))
        for k in jdga.fasta.read(path)
    ]
    return [(bytes.decode(k.name), len(k.seq),
             float(jdga.assembly.gc_content([k.seq])),
             sum(map(jdga.sequence.is_gap, k.seq))) for k in sequences]


def write_stats(out_path: str, stats: List[SeqRecord], label: str) -> None:
    """Write sequence statistics to the specified file."""
    with open(out_path, 'wt') as out_file:
        print('#' + str.join('\t', _COLUMNS), file=out_file)
        for k in stats:
            print(str.format('{:s}\t{:s}\t{:d}\t{:f}\t{:d}', label, k[0], k[1],
                             k[2], k[3]),
                  file=out_file)


if __name__ == '__main__':
    write_stats(snakemake.output[0], seq_stats(snakemake.input[0]),
                snakemake.params.label)
