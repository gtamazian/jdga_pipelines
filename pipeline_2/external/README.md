External programs and datasets
==============================

This directory should contain two files and two directories (symbolic
links also allowed) for the pipeline.

1. The FASTA file of the NCBI RefSeq domestic cat genome assembly
   Felis catus 9.0:

   https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/181/335/GCF_000181335.3_Felis_catus_9.0/GCF_000181335.3_Felis_catus_9.0_genomic.fna.gz

2. The GFF3 file of the NCBI genome annotation for the Felis catus 9.0
   assembly of the domestic cat genome:

   https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/181/335/GCF_000181335.3_Felis_catus_9.0/GCF_000181335.3_Felis_catus_9.0_genomic.gff.gz

3. The directory with the OrthoDB v10.1 database file set:

	https://www.orthodb.org/?page=filelist

4. The directory of the SnpEff program:

	http://pcingola.github.io/SnpEff/
