JDGA Pipelines
==============

This repository contains two [Snakemake](https://snakemake.github.io/)
pipelines that define the data processing for paper

Gaik Tamazian, Pavel Dobrynin, Anna Zhuk, Daria V Zhernakova, Polina L
Perelman, Natalia A Serdyukova, Alexander S Graphodatsky, Aleksey
Komissarov, Sergei Kliver, Nikolay Cherkasov, Alan F Scott, David W
Mohr, Klaus-Peter Koepfli, Stephen J O’Brien, Ksenia Krasheninnikova,
Draft *de novo* Genome Assembly of the Elusive Jaguarundi, *Puma
yagouaroundi*, *Journal of Heredity*, 2021.

https://doi.org/10.1093/jhered/esab036

The first pipeline covers read alignment and variant calling. The
second pipeline describes how display items for the paper were
derived.

The pipelines require the JDGA package, which is available in the
separate [GitLab repository](https://gitlab.com/gtamazian/jdga).
